package tomsksoft.jd.managertree.delegated_adapter

object AdapterConstants{
    val BASE = 0
    val NODE = 1
    val LEAF = 2
}
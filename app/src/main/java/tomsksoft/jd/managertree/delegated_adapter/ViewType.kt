package tomsksoft.jd.managertree.delegated_adapter

interface ViewType {
    fun getViewType(): Int
}